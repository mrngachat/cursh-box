import React, { Component } from 'react';
import './App.css';
import api from './api'
import { Button, Badge, Paper, Fab } from '@material-ui/core'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import MoveToInboxIcon from '@material-ui/icons/MoveToInbox';
import ChatIcon from '@material-ui/icons/Chat';
import SettingsBackupRestoreIcon from '@material-ui/icons/SettingsBackupRestore';

class App extends Component {

   constructor(props) {
      super(props)
      this.state = {
         name: null,
         quiz_id: null,
         page: 'home',
         preloadedRewardedVideo: null,
         preloadedInterstitial: null,
         user_id: null,
         user_data: { id: '', gender: '', playerId: '', playerName: '', playerPhoto: '', lang: '', language: 'english', friendsList: [] },
         showBlock2: false,
         showBlock1: true,
         showBlock2: false

      }
   }

   componentDidMount() {

      let { user_data } = this.state

      const script = document.createElement('script')
      script.src = 'https://connect.facebook.net/en_US/fbinstant.6.3.js'
      script.id = 'fbinstant'
      document.body.appendChild(script)
      let self = this
      script.onload = () => {
         window.FBInstant.setLoadingProgress(50);
         window.FBInstant.initializeAsync().then(function () {
            window.FBInstant.setLoadingProgress(100);
            window.FBInstant.startGameAsync().then(async () => {
               const entryPointData = window.FBInstant.getEntryPointData()
               const fbid = window.FBInstant.player.getID()
               const name = window.FBInstant.player.getName()
               const profile = window.FBInstant.player.getPhoto()
               console.log(fbid)

               user_data.playerId = fbid
               user_data.playerName = name
               user_data.playerPhoto = profile
               let body = {}
               body.fbid = fbid
               body.name = name
               if (entryPointData) {
                  body.from_play_id = entryPointData.play_id
                  body.from_user_id = entryPointData.user_id
                  body.from_datetime = entryPointData.timestamp
               }


               self.setState({ user_data })

               self.getFriends(function () {
                  console.log(user_data);

               });


               // const res = await api.post('login', body)
               // const user_id = res.data
               // self.preloadVideo()
               // self.preloadInterstitial()

               if (entryPointData) {
                  self.setState({ page: 'quiz', quiz_id: entryPointData.id })
               } else {
                  self.setState({ page: 'home' })
               }
               // self.setState({ name, profile, fbid, user_id })



            });
         });
      }


      // let { userData } = this.state

      // user_data.playerId = '2945856565436804'
      // user_data.playerPhoto = "https://platform-lookaside.fbsbx.com/platform/instantgames/profile_pic.jpg?igpid=2945856565436804&height=256&width=256&ext=1598803568&hash=AeQN_KF7iRWfBSuv"
      // user_data.playerName = 'Aye Thitar Phyo'

      this.setState({ user_data })




   }


   // getFriend = () => {
   //    let { user_data } = this.state

   //    window.FBInstant.player.getConnectedPlayersAsync().then(function (players) {
   //       players = players.splice(0, 2);
   //       players.map(function (player) {
   //          user_data.friendsList.push({ 'id': player.getID(), 'name': player.getName(), 'photo': player.getPhoto() });
   //       });


   //    }).then(function () {
   //       console.log("friends list ready")
   //    }).catch(function (e) {
   //       console.log("friends data failed", e)
   //    });
   // }


   getFriends = (callback) => {
      let { user_data } = this.state


      window.FBInstant.player.getConnectedPlayersAsync().then(function (players) {
         //players = players.splice(0,2);
         //console.log('players '+JSON.stringify(players));
         players.map(function (player) {
            user_data.friendsList.push({ 'id': player.getID(), 'name': player.getName(), 'photo': player.getPhoto() });
         });
         return callback();
      }).then(function () {
         console.log("friends list ready")
      }).catch(function (e) {
         console.log("friends data failed", e)
      });
   }



   preloadVideo = () => {
      let self = this
      // if(process.env.NODE_ENV !== 'production') {
      //    return
      // }
      window.FBInstant.getRewardedVideoAsync(
         '266632380926503_611490573107347'
      ).then(function (rewarded) {
         let preloadedRewardedVideo = rewarded
         self.setState({ preloadedRewardedVideo })
         return preloadedRewardedVideo.loadAsync();
      }).then(function () {
         console.log('Rewarded video preloaded');
      }).catch(function (err) {
         self.setState({ preloadedRewardedVideo: null })
         console.error('Rewarded video failed to preload: ' + err.message);
      });

   }
   preloadInterstitial = () => {
      // if(process.env.NODE_ENV !== 'production') {
      //    return
      // }
      let self = this
      window.FBInstant.getInterstitialAdAsync(
         '266632380926503_622658321990572'
      ).then(function (interstitial) {
         let preloadedInterstitial = interstitial;
         self.setState({ preloadedInterstitial })
         return preloadedInterstitial.loadAsync();
      }).then(function () {
         console.log('Interstitial preloaded');
      }).catch(function (err) {
         self.setState({ preloadedInterstitial: null })
         console.error('Interstitial failed to preload: ' + err.message);
         console.log('Interstitial failed to preload: ' + err.message);

      });

   }

   onHelp = async () => {
      let self = this

      const { preloadedRewardedVideo } = this.state
      if (preloadedRewardedVideo) {
         preloadedRewardedVideo.showAsync()
            .then(async () => {
               console.log('Rewarded video watched successfully');
               // await api.post('help', { user_id })
               this.preloadVideo()
            })
            .catch(function (e) {
               this.showInterstitial()
               console.error(e.message);
            });
      } else {
         this.showInterstitial()
      }
   }
   showInterstitial = () => {
      let self = this
      const { user_id } = this.state
      const { preloadedInterstitial } = this.state
      if (preloadedInterstitial) {
         preloadedInterstitial.showAsync()
            .then(async () => {
               await api.post('help', { user_id })
               console.log('Interstitial ad finished successfully');
               this.preloadInterstitial()
            })
            .catch(function (e) {
               console.error(e.message);
            });
      }
   }

   onChangePage = (page, quiz_id = null) => {

      this.setState({ page, quiz_id })
   }

   onSend = () => {
      this.setState({ showBlock2: true })

      setTimeout(() => {
         this.Block2.scrollIntoView({ behavior: "smooth" });
      }, 1)

      setTimeout(() => {
         this.setState({ showBlock1: false })
      }, 1000)



   }


   backToBlock1 = () => {
      this.setState({ showBlock1: true })
      setTimeout(() => {
         this.Block1.scrollIntoView({ behavior: "smooth" });
      }, 1)
      setTimeout(() => {
         this.setState({ showBlock2: false })
      }, 1000)
   }


   onSend2 = () => {
      this.setState({ showBlock3: true })
      setTimeout(() => {
         this.Block3.scrollIntoView({ behavior: "smooth" });
      }, 1)

      setTimeout(() => {
         this.setState({ showBlock2: false })
      }, 500)

   }
   render() {
      const { user_data, showBlock2, showBlock1, showBlock3 } = this.state
      // console.log(user_data)
      return (
         <div id="app">
            <div id="header" onClick={() => this.onChangePage('home')} >
               <img style={{ width: 35 }} src={require('./images/logo.png')} />
            </div>

            {showBlock1 &&
               <div id='block-1' style={{ height: window.innerHeight, backgroundColor: 'grey' }} >
                  <div style={{ textAlign: 'center' }} >
                     <img style={{ width: 100, height: 100, borderRadius: 50, marginTop: 20 }} src={user_data.playerPhoto} />
                     <p id='user-name' >{user_data.playerName}</p>
                  </div>

                  <div style={{ paddingLeft: 50, paddingRight: 50, marginTop: 90 }} >
                     <div style={{ width: '100%' }} >

                        <Button onClick={this.onSend} style={{ width: '100%', height: 60, fontFamily: 'EB Garamond', fontSize: 14, fontWeight: 'bold' }} variant="contained" startIcon={<MailOutlineIcon style={{ fontSize: 40 }} />} >Send to your crush </Button>

                     </div>
                     <div style={{ width: '100%', marginTop: 30 }} >
                        <Badge badgeContent={4} color="primary"  >
                           <Button style={{ width: '100%', height: 60, fontFamily: 'EB Garamond', fontSize: 14, fontWeight: 'bold' }} variant="contained" startIcon={<MoveToInboxIcon style={{ fontSize: 40 }} />} >Read Message</Button>
                        </Badge>
                     </div>
                  </div>
               </div>


            }

            {showBlock2 &&
               <div ref={(el) => { this.Block2 = el; }} id='block-2' style={{ backgroundColor: 'grey', height: window.innerHeight }}>
                  {user_data.friendsList.length > 0 && user_data.friendsList.map((item, index) => (

                     <div key={index} style={{ padding: 1, display: 'flex', alignItems: 'center' }} onClick={this.onSend2} >
                        <Paper style={{ width: '100%', display: 'flex', padding: 10, justifyContent: 'space-between', alignItems: 'center' }} >
                           <div style={{ display: 'flex', alignItems: 'center' }} >
                              <img style={{ width: 70, borderRadius: 35 }} src={item.photo} />
                              <p style={{ fontSize: 20, marginLeft: 10 }} >{item.name}</p>
                           </div>
                           <ChatIcon style={{ fontSize: 25 }} />
                        </Paper>
                     </div>

                  ))}
                  <div style={{
                     position: 'fixed',
                     bottom: 20,
                     right: 20
                  }} >
                     <Fab onClick={this.backToBlock1} color="primary" aria-label="add">
                        <SettingsBackupRestoreIcon style={{ fontSize: 25 }} />
                     </Fab>
                  </div>

               </div>
            }


            {showBlock3 &&
               <div ref={(el) => { this.Block3 = el; }} id='block-3' style={{ backgroundColor: 'green', height: window.innerHeight }}>


               </div>
            }


         </div>
      );
   }
}

export default App;
