import React, { Component } from 'react'
import api from '../api'


class Home extends Component {
   constructor(props) {
      super(props)
      this.state = {
         quizzes: null,
         offset: 0,
         end: false,
         isLoading: false

      }
   }

   componentDidMount() {
      this.onStart()
      setTimeout(() => {
         window.scrollTo(0, 0)
         document.getElementById('home').scrollTo(0, 0)
      }, 100)
   }

   onStart = async () => {
      this.setState({ isLoading: true })
      const quizes_res = await api.get('/quizzes', { 'lang': 'eng', 'page': 0 })
      console.log(quizes_res.data)
      if (quizes_res.data.last) {
         this.setState({
            end: true
         })
      }

      let data = quizes_res.data.data

      this.setState({
         quizzes: data,
         isLoading: false
      })
   }

   loadMore = async () => {
      let { offset, quizzes } = this.state
      offset += 1
      const quizes_res = await api.get('/quizzes?lang=eng&page=' + offset)
      console.log(quizes_res.data)
      if (quizes_res.data.last) {
         this.setState({
            end: true
         })
      }

      let data = quizes_res.data.data

      quizzes = [...quizzes, ...data]

      this.setState({
         quizzes,
         offset
      })
   }

   onClick = (i) => {
      this.props.changePage('quiz', i)
   }

   render() {
      let { quizzes, end, isLoading } = this.state

      return (
         <div id="home" >

            {isLoading ?
               <div style={{ backgroundColor:'white', height: window.innerHeight,display:'flex',alignItems:'center',justifyContent:'center' }} >
                  <img width='90px' src={require('../images/loading.svg')} />
               </div>
               :
               <>
                  {quizzes &&
                     quizzes.map((quiz, i) => (
                        <div className="quizzes" key={i} onClick={() => this.onClick(quiz._id)} >
                           <img style={{ marginTop: 10 }}  onError={(e)=>{e.target.onError = null; e.target.src="https://mrngachat.top/pl.png"}} src={quiz.image} />
                           {quiz.name}
                        </div>
                     ))
                  }
                  {!end &&
                     <div style={{ textAlign: 'center' }} onClick={this.loadMore} >
                        <p style={{
                           padding: 10,
                           color: 'white',
                           backgroundColor: 'red',
                           borderRadius: 10
                        }}>Load More</p>
                     </div>
                  }

               </>}
         </div>
      )
   }

}

export default Home
