import React, { Component } from 'react'

import api from '../api'
import axios from 'axios';
import Loader from './Loader'


class Quiz extends Component {

   constructor(props) {
      super(props)
      // this.Ans = React.createRef();
      this.state = {
         id: props.id,
         user_id: props.user_id,
         play_id: null,
         quiz: null,
         config: null,
         w: window.innerWidth,
         random_answer: null,
         userData: props.userData,
         normalData: '',
         isLoading: false,
         type: 'normal',
         chooseGen: false,
         gender: '',
         imgLoading: false,
         related: null,
         pickGender: false
      }
   }

   async componentDidMount() {
      setTimeout(() => {
         this.showInterstitial()
      }, 1000)




      const { id, user_id } = this.state


      this.onStart(id)

   }

   onStart = async (id) => {
      // let { refreshInterstitialAds, refreshVideoAds } = this.props
      // refreshInterstitialAds()
      // refreshVideoAds()

      let quiz_res = await api.get('/quiz?id=' + id)
      let data = quiz_res.data.data

      let _res = await api.get('/related?id=' + id)
      let related = _res.data.data

      this.setState({
         quiz: data,
         related
      })
      if (!data.friends && !data.gender) {
         this.normalQuiz()
         this.setState({ type: 'normal', isLoading: false, pickGender: false })
         setTimeout(() => {
            this.Ans.scrollIntoView({ behavior: "smooth" });
         }, 500)
      }

      if (!data.friends && data.gender) {
         this.setState({ type: 'gender', isLoading: false })
         if (this.state.gender != '') {
            this.setState({ pickGender: false })
            this.genderQuizz()

         } else {
            this.setState({ pickGender: true })

            setTimeout(() => {
               this.Gen.scrollIntoView({ behavior: "smooth" });
            }, 500)
         }

      }

   }

   normalQuiz = async () => {
      this.setState({ imgLoading: true })
      let { userData, id } = this.state

      const data = new FormData()
      data.append('data[id]', id)
      data.append('data[gender]', userData.gender)
      data.append('data[playerId]', userData.playerId)
      data.append('data[playerName]', userData.playerName)
      data.append('data[playerPhoto]', userData.playerPhoto)
      data.append('data[lang]', userData.lang)
      data.append('data[language]', userData.language)

      let a = this

      axios({
         method: 'post',
         url: 'https://mrngachat.top/api/result',
         data: data,
         headers: { 'Content-Type': 'multipart/form-data' }
      })
         .then(function (response) {
            //handle success
            a.setState({
               normalData: response.data.image,
               imgLoading: false
            })

            setTimeout(() => {
               a.Ans.scrollIntoView({ behavior: "smooth" });
            }, 1)

         })
         .catch(function (response) {

            console.log(response);
         });

   }

   genderQuizz = async () => {
      this.setState({ imgLoading: true })
      let { userData, id, gender } = this.state

      const data = new FormData()
      data.append('data[id]', id)
      data.append('data[gender]', userData.gender)
      data.append('data[playerId]', userData.playerId)
      data.append('data[playerName]', userData.playerName)
      data.append('data[playerPhoto]', userData.playerPhoto)
      data.append('data[lang]', userData.lang)
      data.append('data[language]', userData.language)
      data.append('data[gender]', gender)
      let a = this
      axios({
         method: 'post',
         url: 'https://mrngachat.top/api/result',
         data: data,
         headers: { 'Content-Type': 'multipart/form-data' }
      })
         .then(function (response) {
            a.setState({
               normalData: response.data.image,
               imgLoading: false
            })
            setTimeout(() => {
               a.Ans.scrollIntoView({ behavior: "smooth" });
            }, 1)

         })
         .catch(function (response) {

            console.log(response);
         });
   }


   chooseGender = (data) => {


      this.setState({ gender: data, chooseGen: true })

      setTimeout(() => {
         this.Ans.scrollIntoView({ behavior: "smooth" });
      }, 1)

      this.genderQuizz()


   }


   componentWillReceiveProps(nextProps) {
      if (nextProps.id !== this.state.id) {
         this.setState({
            id: nextProps.id,
            normalData: null,
            imgLoading: true,
            chooseGen: false,
            gender: '',
            quiz: null
         })
         this.onStart(nextProps.id)
      }
   }


   onTryAgain = async () => {
      setTimeout(() => {
         this.showInterstitial()
      }, 500)


      let { id, type } = this.state
      setTimeout(() => {
         this.Ans.scrollIntoView({ behavior: "smooth" });
      }, 1)

      this.setState({
         normalData: null,
         imgLoading: true,

      })
      // this.onStart(id)
      if (type == 'gender') {
         this.genderQuizz()

      } else if (type == 'normal') {
         this.normalQuiz()
      }
   }

   toQuiz = (i) => {

      this.props.changePage('quiz', i)
   }

   onShare = () => {
      const { normalData, id, userData, play_id } = this.state

      let today = new Date()
      let datetime = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()

      setTimeout(() => {
         this.showVideo()
      }, 500)

      window.FBInstant.shareAsync({
         intent: "SHARE",
         image: 'data:image/png;base64,' + normalData,
         text: "Play OMG NOW",
         data: { "id": id }
      })
         .then(async () => {
            console.log('shared')
            this.Related.scrollIntoView({ behavior: "smooth" });
            setTimeout(() => {
               this.showVideo()
            }, 500)


         })
         .catch(function (e) {
            console.log('error sharing')
            console.log(e)
         });
   }


   showInterstitial = () => {
      let self = this
      const { preloadedInterstitial, refreshInterstitialAds } = this.props
      if (preloadedInterstitial) {
         preloadedInterstitial.showAsync()
            .then(async () => {
               console.log('Interstitial ad finished successfully');
               refreshInterstitialAds()
            })
            .catch(function (e) {
               console.log('Inter Ad failed in Quiz.js', e)
               console.error(e.message);
            });
      }
   }
   showVideo = () => {
      // let self = this
      // const { preloadedRewardedVideo, refreshVideoAds } = this.props
      // if (preloadedRewardedVideo) {
      //    preloadedRewardedVideo.showAsync()
      //       .then(async () => {
      //          console.log('Rewarded video watched successfully');
      //          refreshVideoAds()
      //          setTimeout(() => {
      //             this.showInterstitial()
      //          }, 500)
      //       })
      //       .catch(function (e) {
      //          this.showInterstitial()
      //          console.error(e.message);
      //       });
      // } else {
      //    this.showInterstitial()
      // }
      this.props.showRe()
   }



   render() {
      const { quiz, gender, imgLoading, chooseGen, pickGender, isLoading, type, related } = this.state

      return (
         <div id="quiz" >
            {isLoading ?
               <Loader />
               :
               <>

                  {quiz &&
                     <div>
                        {/* <div>{quiz && quiz.name}</div> */}
                        {pickGender &&
                           <div style={{ height: 300 }} ref={(el) => { this.Gen = el; }} >
                              <div style={{ backgroundColor: 'black' }} >
                                 <p style={{ padding: 15, color: 'white' }} >က်ား / မ ေရြးပါ</p>
                              </div>
                              <div onClick={() => this.chooseGender('male')}
                                 style={{
                                    border: '1px solid grey',
                                    marginTop: -10,
                                    backgroundColor: gender == 'male' ? '#3474eb' : 'white',
                                    height: 40, alignItems: 'center',
                                    justifyContent: 'center',
                                    display: 'flex'
                                 }}>
                                 <p style={{
                                    color: gender == 'male' ? 'white' : 'black'
                                 }} >က်ား</p>
                              </div>


                              <div onClick={() => this.chooseGender('female')}
                                 style={{
                                    border: '1px solid grey',
                                    marginTop: 3,
                                    backgroundColor: gender == 'female' ? '#3474eb' : 'white',
                                    height: 40,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    display: 'flex'
                                 }}>
                                 <p style={{
                                    color: gender == 'female' ? 'white' : 'black'
                                 }}>မ</p>
                              </div>
                           </div>
                        }
                        {chooseGen &&
                           <div>
                              {imgLoading ?
                                 <div ref={(el) => { this.Ans = el; }} >

                                    <button style={{ fontFamily: 'Zawgyi' }} className="btn-fb-share" >{quiz && quiz.name}</button>
                                    <div style={{ height: 500, backgroundColor: 'white', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }} >
                                       <img width='80' src={require('../images/loading.gif')} />
                                       <p style={{ color: 'black' }} >ပံုေလးကို ႏွိပ္ၿပီး Share သြားဖို႔ မေမ့နဲ႔ေနာ္ ...</p>
                                    </div>
                                    <button id="try-again" onClick={this.onTryAgain} >Try Again</button>
                                    <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>

                                 </div>
                                 :
                                 <div ref={(el) => { this.Ans = el; }} >
                                    <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>

                                    {/* <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button> */}
                                    <img width='100%' src={'data:image/png;base64,' + this.state.normalData} />
                                    <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>
                                    <button id="try-again" onClick={this.onTryAgain} >Try Again</button>
                                 </div>
                              }

                           </div>

                        }
                        {type == 'normal' &&
                           <div>
                              {imgLoading ?
                                 <div ref={(el) => { this.Ans = el; }} >

                                    <button style={{ fontFamily: 'Zawgyi' }} className="btn-fb-share" >{quiz && quiz.name}</button>
                                    <div style={{ height: 500, backgroundColor: 'white', display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }} >
                                       <img width='80' src={require('../images/loading.gif')} />
                                       <p style={{ color: 'black' }} >ပံုေလးကို ႏွိပ္ၿပီး Share သြားဖို႔ မေမ့နဲ႔ေနာ္ ...</p>
                                    </div>
                                    <button id="try-again" onClick={this.onTryAgain} >Try Again</button>
                                    <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>

                                 </div>
                                 :
                                 <div ref={(el) => { this.Ans = el; }} >
                                    <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>
                                    <img width='100%' src={'data:image/png;base64,' + this.state.normalData} />
                                    <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>
                                    <button id="try-again" onClick={this.onTryAgain} >Try Again</button>

                                 </div>
                              }

                           </div>
                        }
                     </div>


                  }
                  <div ref={(el) => { this.Related = el; }} />
                  {related &&
                     related.map((quiz, i) => (
                        <div className="quizzes" key={i} onClick={() => this.toQuiz(quiz._id)} >
                           <img style={{ marginTop: 10 }} onError={(e) => { e.target.onError = null; e.target.src = "https://mrngachat.top/pl.png" }} src={quiz.image} />
                           {quiz.name}
                        </div>
                     ))
                  }

               </>
            }



            {/* {quiz &&
               <div>
                  <div>{quiz.name}</div>
                  <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>
                  <canvas id="canvas" width="600" height="900" ></canvas>

                  {share_image ?
                     <img src={share_image} width="100%" />
                     :
                     <div id="loading" style={{ width: `${w - 20}`, height: (w-20)* (900/600),  }} >
                     ခ ဏ စောင့် ပါ ...
                  </div>
               }

               <button className="btn-fb-share" onClick={this.onShare}>Share on Facebook</button>
               <button id="try-again" onClick={this.onTryAgain} >Try Again</button>

               <hr />
               <h3>တခြား Game များ</h3>
               {Quizzes.filter( q => q.slug != id ).map( (quiz, i) => (
                  <div className="quizzes" key={i} onClick={()=>this.toQuiz(quiz.slug)} >
                     <img src={ require(`images/${quiz.slug}/thumbnail.jpg`) } />
                     {quiz.name}
                  </div>
               ))}
            </div>
         } */}
         </div>
      )
   }

   shuffleArray = (array) => {
      for (let i = array.length - 1; i > 0; i--) {
         const j = Math.floor(Math.random() * (i + 1));
         [array[i], array[j]] = [array[j], array[i]];
      }
   }

}

export default Quiz
