import React, { Component, Fragment } from 'react'



class Com extends Component {

  constructor() {
    super()
    this.state = {
      width: 0,
      height: 0,
    }
  }

  componentDidMount() {
    let width = window.innerWidth
    let height = window.innerHeight
    this.setState({ width, height })
  }

  render() {
    const {width, height} = this.state

    return (
        <div style={{
            position: 'absolute',
            left: 0,
            top: 0,
            width: width,
            height: height,
            backgroundColor: '#00000022',
            zIndex: 2000,
        }} >
            <div  style={{
                flex:1,
                backgroundColor: 'white',
                width: 100,
                height: 100,
                alignSelf: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 12,
                },
                shadowOpacity: 0.58,
                shadowRadius: 16.00,
                elevation: 24,
                position: 'absolute',
                top: (height / 2) - 150,
                left: (width / 2) - 50,
                zIndex: 1000,
                display: 'flex',
                alignItems : 'center',


            }} >
                <div>Loading</div>
            </div>
        </div>
    )

  }

}

export default Com
